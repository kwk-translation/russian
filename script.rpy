﻿# TODO: Translation updated at 2017-10-07 10:36

# /home/max/Bureau/script.rpyc:221
translate russian splashscreen_73841dfc:

    # centered "THIS IS AN ALPHA RELEASE\n\nIt's not meant for public.\nPlease do not distribute!{fast}"
    centered "ЭТО ВСЕГО ЛИШЬ АЛЬФА-ВЕРСИЯ\n\nОна не предназначена для публичного использования.\nПожалуйста, не копируйте и не распространяйте её!{fast}"

# /home/max/Bureau/script.rpyc:252
translate russian konami_code_e5b259c9:

    # "Max le Fou" "What the heck am I doing here?..."
    "Max le Fou" "Какого хрена Я здесь делаю?..."

# /home/max/Bureau/script.rpyc:287
translate russian gjconnect_4ae60ea0:

    # "Disconnected."
    "Соединение прекращено."

# /home/max/Bureau/script.rpyc:316
translate russian gjconnect_e254a406:

    # "Connection to Game Jolt succesful.\nWARNING: The connection doesn't count for previously saved games."
    "Успешное соединение с сервисом Game Jolt.\nПРЕДУПРЕЖДЕНИЕ: Соединение не засчитает ранее сохранённые игры."

translate russian strings:

    # script.rpy:200
    old "Master"
    new "Мастер"

    # script.rpy:201
    old "Big brother"
    new "Большой брат"

    # script.rpy:272
    old "Disconnect from Game Jolt?"
    new "Отсоединиться от сервиса Game Jolt?"

    # script.rpy:272
    old "Yes, disconnect."
    new "Да, отсоединиться."

    # script.rpy:272
    old "No, return to menu."
    new "Нет, вернуться в главное меню."

    # script.rpy:334
    old "A problem occured. Maybe your connection has a problem. Or maybe it's Game Jolt derping...\n\nTry again?"
    new "Произошла ошибка. Проверьте качество связи с сетью интернет. Но может быть и так, что Game Jolt тупит...\n\nПопробовать снова?"

    # script.rpy:334
    old "Yes, try again."
    new "Да, попробовать снова."

    # script.rpy:352
    old "It seems the authentication failed. Maybe you didn't write correctly the username and token...\n\nTry again?"
    new "Похоже, аутентификация провалилась. Возможно, что вы неправильно указали имя пользователя или токен...\n\nПопробовать снова?"
    
    # script.rpy:228
    old "Please enter your name and press Enter:"
    new "Введите имя и нажмите Enter.:"
    
    # script.rpy:295
    old "Type here your username and press Enter."
    new "Введите здесь псевдоним Game Jolt и нажмите Enter."
    
    # script.rpy:296
    old "Now, type here your game token and press Enter."
    new "Теперь введите токен Game Jolt и нажмите Enter."
    
    # script.rpy:207
    old "Game Jolt trophy obtained!"
    new "Получен трофей Game Jolt!"
    
    # script.rpy:180
    old "Sakura's mom"
    new "Мать Сакура"
    
    # script.rpy:181
    old "Sakura's dad"
    new "отец Сакура"
    
    # script.rpy:182
    old "Toshio"
    new "Тошио"

    # script.rpy:188
    old "Taichi"
    new "Тайчи"
    
    # script.rpy:188
    old "Girl"
    new "девушка"
    
    # script.rpy:13
    old "{size=80}Thanks for playing!"
    new "{size=80}Спасибо за игру!"

# TODO: Translation updated at 2019-04-23 11:21

# game/script.rpy:286
translate russian update_menu_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"

# game/script.rpy:300
translate russian gjconnect_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"

# TODO: Translation updated at 2019-04-27 11:15

translate russian strings:

    # script.rpy:226
    old "Achievement obtained!"
    new "Achievement obtained!"

