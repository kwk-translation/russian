﻿# TODO: Translation updated at 2017-10-07 10:37

translate russian strings:

    # 00action_file.rpy:26
    old "{#weekday}Monday"
    new "{#weekday}Понедельник"

    # 00action_file.rpy:26
    old "{#weekday}Tuesday"
    new "{#weekday}Вторник"

    # 00action_file.rpy:26
    old "{#weekday}Wednesday"
    new "{#weekday}Среда"

    # 00action_file.rpy:26
    old "{#weekday}Thursday"
    new "{#weekday}Четверг"

    # 00action_file.rpy:26
    old "{#weekday}Friday"
    new "{#weekday}Пятница"

    # 00action_file.rpy:26
    old "{#weekday}Saturday"
    new "{#weekday}Суббота"

    # 00action_file.rpy:26
    old "{#weekday}Sunday"
    new "{#weekday}Воскресенье"

    # 00action_file.rpy:37
    old "{#weekday_short}Mon"
    new "{#weekday_short}Пон"

    # 00action_file.rpy:37
    old "{#weekday_short}Tue"
    new "{#weekday_short}Вт"

    # 00action_file.rpy:37
    old "{#weekday_short}Wed"
    new "{#weekday_short}Ср"

    # 00action_file.rpy:37
    old "{#weekday_short}Thu"
    new "{#weekday_short}Чет"

    # 00action_file.rpy:37
    old "{#weekday_short}Fri"
    new "{#weekday_short}Пят"

    # 00action_file.rpy:37
    old "{#weekday_short}Sat"
    new "{#weekday_short}Суб"

    # 00action_file.rpy:37
    old "{#weekday_short}Sun"
    new "{#weekday_short}Вос"

    # 00action_file.rpy:47
    old "{#month}January"
    new "{#month}Январь"

    # 00action_file.rpy:47
    old "{#month}February"
    new "{#month}Февраль"

    # 00action_file.rpy:47
    old "{#month}March"
    new "{#month}Март"

    # 00action_file.rpy:47
    old "{#month}April"
    new "{#month}Апрель"

    # 00action_file.rpy:47
    old "{#month}May"
    new "{#month}Май"

    # 00action_file.rpy:47
    old "{#month}June"
    new "{#month}Июнь"

    # 00action_file.rpy:47
    old "{#month}July"
    new "{#month}Июль"

    # 00action_file.rpy:47
    old "{#month}August"
    new "{#month}Август"

    # 00action_file.rpy:47
    old "{#month}September"
    new "{#month}Сентябрь"

    # 00action_file.rpy:47
    old "{#month}October"
    new "{#month}Октябрь"

    # 00action_file.rpy:47
    old "{#month}November"
    new "{#month}Ноябрь"

    # 00action_file.rpy:47
    old "{#month}December"
    new "{#month}Декабрь"

    # 00action_file.rpy:63
    old "{#month_short}Jan"
    new "{#month_short}Янв"

    # 00action_file.rpy:63
    old "{#month_short}Feb"
    new "{#month_short}Фев"

    # 00action_file.rpy:63
    old "{#month_short}Mar"
    new "{#month_short}Мар"

    # 00action_file.rpy:63
    old "{#month_short}Apr"
    new "{#month_short}Апр"

    # 00action_file.rpy:63
    old "{#month_short}May"
    new "{#month_short}Май"

    # 00action_file.rpy:63
    old "{#month_short}Jun"
    new "{#month_short}Июн"

    # 00action_file.rpy:63
    old "{#month_short}Jul"
    new "{#month_short}Июл"

    # 00action_file.rpy:63
    old "{#month_short}Aug"
    new "{#month_short}Авг"

    # 00action_file.rpy:63
    old "{#month_short}Sep"
    new "{#month_short}Сен"

    # 00action_file.rpy:63
    old "{#month_short}Oct"
    new "{#month_short}Окт"

    # 00action_file.rpy:63
    old "{#month_short}Nov"
    new "{#month_short}Нояб"

    # 00action_file.rpy:63
    old "{#month_short}Dec"
    new "{#month_short}Дек"

    # 00action_file.rpy:235
    old "%b %d, %H:%M"
    new "%b %d, %H:%M"

    # 00action_file.rpy:820
    old "Quick save complete."
    new "Успешное быстрое сохранение."

    # 00gui.rpy:234
    old "Are you sure?"
    new "Вы уверены?"

    # 00gui.rpy:235
    old "Are you sure you want to delete this save?"
    new "Вы уверены, что хотите удалить данное сохранение?"

    # 00gui.rpy:236
    old "Are you sure you want to overwrite your save?"
    new "Вы уверены, что хотите перезаписать данное сохранение?"

    # 00gui.rpy:237
    old "Loading will lose unsaved progress.\nAre you sure you want to do this?"
    new "При загрузке, весь несохранённый прогресс будет утерян.\nВы уверены, что хотите сделать это?"

    # 00gui.rpy:238
    old "Are you sure you want to quit?"
    new "Вы действительно хотите выйти?"

    # 00gui.rpy:239
    old "Are you sure you want to return to the main menu?\nThis will lose unsaved progress."
    new "Вы уверены, что хотите выйти в главное меню?\nНесохранённый прогресс будет утерян."

    # 00gui.rpy:240
    old "Are you sure you want to end the replay?"
    new "Вы действительно хотите закончить воспроизведение?"

    # 00gui.rpy:241
    old "Are you sure you want to begin skipping?"
    new "Вы уверены, что хотите начать пропуск диалогов?"

    # 00gui.rpy:242
    old "Are you sure you want to skip to the next choice?"
    new "Вы уверены, что хотите перескочить до ближайшего экрана выбора?"

    # 00gui.rpy:243
    old "Are you sure you want to skip unseen dialogue to the next choice?"
    new "Вы уверены, что хотите перескочить неувиденные диалоги и перейти к ближайшему экрану выбора?"

    # 00keymap.rpy:259
    old "Saved screenshot as %s."
    new "Скриншот сохранён как %s."

    # 00library.rpy:142
    old "Self-voicing disabled."
    new "Синтез речи отключён."

    # 00library.rpy:143
    old "Clipboard voicing enabled. "
    new "Озвучка буфера обмена включена. "

    # 00library.rpy:144
    old "Self-voicing enabled. "
    new "Синтез речи включён. "

    # 00library.rpy:179
    old "Skip Mode"
    new "Режим Пропуска"

    # 00library.rpy:262
    old "This program contains free software under a number of licenses, including the MIT License and GNU Lesser General Public License. A complete list of software, including links to full source code, can be found {a=https://www.renpy.org/l/license}here{/a}."
    new "Эта программа содержит бесплатное программное обеспечение по ряду лицензий, включающих также MIT и GNU LGPL. Полный список софта, дополнительно содержащий ссылки на исходные коды, может быть найден {a=https://www.renpy.org/l/license}здесь{/a}."

    # 00preferences.rpy:429
    old "Clipboard voicing enabled. Press 'shift+C' to disable."
    new "Включён синтез речи. Нажмите 'shift+C' для отключения."

    # 00preferences.rpy:431
    old "Self-voicing would say \"[renpy.display.tts.last]\". Press 'alt+shift+V' to disable."
    new "Синтезатор речи начнёт говорить \"[renpy.display.tts.last]\". Нажмите 'alt+shift+V' для отключения."

    # 00preferences.rpy:433
    old "Self-voicing enabled. Press 'v' to disable."
    new "Синтез речи включён. Нажмите 'v' для отключения."

    # 00iap.rpy:217
    old "Contacting App Store\nPlease Wait..."
    new "Присоединение к App Store\nПожалуйста, Ждите..."

    # 00updater.rpy:373
    old "The Ren'Py Updater is not supported on mobile devices."
    new "Пакеты обновлений Ren'Py недоступны на мобильных устройствах."

    # 00updater.rpy:492
    old "An error is being simulated."
    new "Произошла ошибка."

    # 00updater.rpy:668
    old "Either this project does not support updating, or the update status file was deleted."
    new "Данный проект не поддерживает обновления, либо файл обновления был удалён."

    # 00updater.rpy:682
    old "This account does not have permission to perform an update."
    new "Ваш аккаунт не имеет требуемых прав для загрузки обновления."

    # 00updater.rpy:685
    old "This account does not have permission to write the update log."
    new "Ваш аккаунт не имеет требуемых прав для записи в журнале обновлений."

    # 00updater.rpy:710
    old "Could not verify update signature."
    new "Не удалось проверить подпись пакетов обновления."

    # 00updater.rpy:981
    old "The update file was not downloaded."
    new "Обновление не загрузилось."

    # 00updater.rpy:999
    old "The update file does not have the correct digest - it may have been corrupted."
    new "Файл обновления имеет неправильную сборку. Вероятно, он повреждён."

    # 00updater.rpy:1055
    old "While unpacking {}, unknown type {}."
    new "Во время распаковки {}, обнаружился неизвестный файл {}."

    # 00updater.rpy:1399
    old "Updater"
    new "Обновление"

    # 00updater.rpy:1406
    old "An error has occured:"
    new "Произошла ошибка:"

    # 00updater.rpy:1408
    old "Checking for updates."
    new "Проверка обновлений."

    # 00updater.rpy:1410
    old "This program is up to date."
    new "Программа имеет актуальную версию."

    # 00updater.rpy:1412
    old "[u.version] is available. Do you want to install it?"
    new "Доступна [u.version]. Обновить?"

    # 00updater.rpy:1414
    old "Preparing to download the updates."
    new "Подготовка к скачиванию обновлений."

    # 00updater.rpy:1416
    old "Downloading the updates."
    new "Загрузка обновлений."

    # 00updater.rpy:1418
    old "Unpacking the updates."
    new "Распаковка обновлений."

    # 00updater.rpy:1420
    old "Finishing up."
    new "Завершение."

    # 00updater.rpy:1422
    old "The updates have been installed. The program will restart."
    new "Обновления установлены. Программе требуется перезагрузка."

    # 00updater.rpy:1424
    old "The updates have been installed."
    new "Обновления установлены."

    # 00updater.rpy:1426
    old "The updates were cancelled."
    new "Обновление прервано."

    # 00updater.rpy:1441
    old "Proceed"
    new "Продолжить"

    # 00updater.rpy:1444
    old "Cancel"
    new "Отмена"

    # 00gallery.rpy:563
    old "Image [index] of [count] locked."
    new "Изображение [index] из [count] заблокировано."

    # 00gallery.rpy:583
    old "prev"
    new "пред."

    # 00gallery.rpy:584
    old "next"
    new "след."

    # 00gallery.rpy:585
    old "slideshow"
    new "слайдшоу"

    # 00gallery.rpy:586
    old "return"
    new "возврат"

    # 00gltest.rpy:64
    old "Graphics Acceleration"
    new "Графическое Ускорение"

    # 00gltest.rpy:70
    old "Automatically Choose"
    new "Автоматический Выбор"

    # 00gltest.rpy:75
    old "Force Angle/DirectX Renderer"
    new "Визуализаторы Force Angle/DirectX"

    # 00gltest.rpy:79
    old "Force OpenGL Renderer"
    new "Визуализатор Force OpenGL"

    # 00gltest.rpy:83
    old "Force Software Renderer"
    new "Визуализатор Force Software"

    # 00gltest.rpy:93
    old "Enable"
    new "Включить"

    # 00gltest.rpy:109
    old "Changes will take effect the next time this program is run."
    new "Изменения вступят в силу после перезагрузки программы."

    # 00gltest.rpy:119
    old "Return"
    new "Возврат"

    # 00gltest.rpy:141
    old "Performance Warning"
    new "Предупреждение о производительности"

    # 00gltest.rpy:146
    old "This computer is using software rendering."
    new "Компьютер использует визуализирующий софт."

    # 00gltest.rpy:148
    old "This computer is not using shaders."
    new "Компьютер не использует шейдеры."

    # 00gltest.rpy:150
    old "This computer is displaying graphics slowly."
    new "Компьютер отображает графику медленно."

    # 00gltest.rpy:152
    old "This computer has a problem displaying graphics: [problem]."
    new "Компьютер имеет проблему с выводом графики: [problem]."

    # 00gltest.rpy:157
    old "Its graphics drivers may be out of date or not operating correctly. This can lead to slow or incorrect graphics display. Updating DirectX could fix this problem."
    new "Графические драйверы устарели или работают неправильно. Это может привести к медленному или некорректному отображению графики. Обновление DirectX должно решить эту проблему."

    # 00gltest.rpy:159
    old "Its graphics drivers may be out of date or not operating correctly. This can lead to slow or incorrect graphics display."
    new "Графические драйверы устарели или работают неправильно. Это может привести к медленному или некорректному отображению графики."

    # 00gltest.rpy:164
    old "Update DirectX"
    new "Обновить DirectX"

    # 00gltest.rpy:170
    old "Continue, Show this warning again"
    new "Продолжить, показать это предупреждение в следующий раз"

    # 00gltest.rpy:174
    old "Continue, Don't show warning again"
    new "Продолжить, не показывать это предупреждение в следующий раз"

    # 00gltest.rpy:192
    old "Updating DirectX."
    new "Обновление DirectX."

    # 00gltest.rpy:196
    old "DirectX web setup has been started. It may start minimized in the taskbar. Please follow the prompts to install DirectX."
    new "Была запущена веб-настройка DirectX. Она могла быть свёрнута в панель задач. Следуйте дальнейшим инструкциям, чтобы установить DirectX."

    # 00gltest.rpy:200
    old "{b}Note:{/b} Microsoft's DirectX web setup program will, by default, install the Bing toolbar. If you do not want this toolbar, uncheck the appropriate box."
    new "{b}Примечание:{/b} DirectX от Майкрософт, помимо себя, по умолчанию устанавливает Бинг в панель задач. Если вы этого не хотите, то снимите соответствующий флажок."

    # 00gltest.rpy:204
    old "When setup finishes, please click below to restart this program."
    new "Когда установка будет завершена, пожалуйста, нажмите ниже для перезагрузки приложения."

    # 00gltest.rpy:206
    old "Restart"
    new "Заново"

    # 00gamepad.rpy:32
    old "Select Gamepad to Calibrate"
    new "Выберите Геймпад для Калибровки"

    # 00gamepad.rpy:35
    old "No Gamepads Available"
    new "Геймпадов не обнаружено"

    # 00gamepad.rpy:54
    old "Calibrating [name] ([i]/[total])"
    new "Калибровка [name] ([i]/[total])"

    # 00gamepad.rpy:58
    old "Press or move the [control!r] [kind]."
    new "Нажмите клавишу или передвиньте стик [control!r] [kind]."

    # 00gamepad.rpy:66
    old "Skip (A)"
    new "Пропуск (A)"

    # 00gamepad.rpy:69
    old "Back (B)"
    new "Назад (B)"

    # _errorhandling.rpym:519
    old "Open"
    new "Открыть"

    # _errorhandling.rpym:521
    old "Opens the traceback.txt file in a text editor."
    new "Открывает файл traceback.txt в текстовом редакторе."

    # _errorhandling.rpym:523
    old "Copy"
    new "Скопировать"

    # _errorhandling.rpym:525
    old "Copies the traceback.txt file to the clipboard."
    new "Копирует файл traceback.txt в буфер обмена."

    # _errorhandling.rpym:543
    old "An exception has occurred."
    new "Неизвестная ошибка."

    # _errorhandling.rpym:562
    old "Rollback"
    new "Отменить"

    # _errorhandling.rpym:564
    old "Attempts a roll back to a prior time, allowing you to save or choose a different choice."
    new "Производит попытку возврата на время назад, позволяя сохраниться или выбрать альтернативный вариант."

    # _errorhandling.rpym:567
    old "Ignore"
    new "Игнорировать"

    # _errorhandling.rpym:569
    old "Ignores the exception, allowing you to continue. This often leads to additional errors."
    new "Игнорирует неизвестные ошибки, позволяя продолжить. Это часто приводит к дополнительным сбоям"

    # _errorhandling.rpym:572
    old "Reload"
    new "Перезапуск"

    # _errorhandling.rpym:574
    old "Reloads the game from disk, saving and restoring game state if possible."
    new "Перезагружает игру с диска, по возможности сохраняя и восстанавливая прежнее состояние."

    # _errorhandling.rpym:576
    old "Console"
    new "Консоль"

    # _errorhandling.rpym:578
    old "Opens a console to allow debugging the problem."
    new "Открывает консоль, делая возможным найти и устранить ошибку."

    # _errorhandling.rpym:590
    old "Quits the game."
    new "Выходит из игры."

    # _errorhandling.rpym:614
    old "Parsing the script failed."
    new "Анализ скрипта не выполнен."

    # _errorhandling.rpym:640
    old "Opens the errors.txt file in a text editor."
    new "Открывает файл errors.txt в текстовом редакторе."

    # _errorhandling.rpym:644
    old "Copies the errors.txt file to the clipboard."
    new "Копирует файл errors.txt в буфер обмена."

# TODO: Translation updated at 2019-04-23 11:21

translate russian strings:

    # 00accessibility.rpy:76
    old "Font Override"
    new "Font Override"

    # 00accessibility.rpy:80
    old "Default"
    new "Default"

    # 00accessibility.rpy:84
    old "DejaVu Sans"
    new "DejaVu Sans"

    # 00accessibility.rpy:88
    old "Opendyslexic"
    new "Opendyslexic"

    # 00accessibility.rpy:94
    old "Text Size Scaling"
    new "Text Size Scaling"

    # 00accessibility.rpy:100
    old "Reset"
    new "Reset"

    # 00accessibility.rpy:105
    old "Line Spacing Scaling"
    new "Line Spacing Scaling"

    # 00accessibility.rpy:117
    old "Self-Voicing"
    new "Self-Voicing"

    # 00accessibility.rpy:121
    old "Off"
    new "Off"

    # 00accessibility.rpy:125
    old "Text-to-speech"
    new "Text-to-speech"

    # 00accessibility.rpy:129
    old "Clipboard"
    new "Clipboard"

    # 00accessibility.rpy:133
    old "Debug"
    new "Debug"

    # 00action_file.rpy:353
    old "Save slot %s: [text]"
    new "Save slot %s: [text]"

    # 00action_file.rpy:434
    old "Load slot %s: [text]"
    new "Load slot %s: [text]"

    # 00action_file.rpy:487
    old "Delete slot [text]"
    new "Delete slot [text]"

    # 00action_file.rpy:569
    old "File page auto"
    new "File page auto"

    # 00action_file.rpy:571
    old "File page quick"
    new "File page quick"

    # 00action_file.rpy:573
    old "File page [text]"
    new "File page [text]"

    # 00action_file.rpy:772
    old "Next file page."
    new "Next file page."

    # 00action_file.rpy:845
    old "Previous file page."
    new "Previous file page."

    # 00action_file.rpy:924
    old "Quick save."
    new "Quick save."

    # 00action_file.rpy:943
    old "Quick load."
    new "Quick load."

    # 00action_other.rpy:355
    old "Language [text]"
    new "Language [text]"

    # 00director.rpy:708
    old "The interactive director is not enabled here."
    new "The interactive director is not enabled here."

    # 00director.rpy:1481
    old "⬆"
    new "⬆"

    # 00director.rpy:1487
    old "⬇"
    new "⬇"

    # 00director.rpy:1551
    old "Done"
    new "Done"

    # 00director.rpy:1561
    old "(statement)"
    new "(statement)"

    # 00director.rpy:1562
    old "(tag)"
    new "(tag)"

    # 00director.rpy:1563
    old "(attributes)"
    new "(attributes)"

    # 00director.rpy:1564
    old "(transform)"
    new "(transform)"

    # 00director.rpy:1589
    old "(transition)"
    new "(transition)"

    # 00director.rpy:1601
    old "(channel)"
    new "(channel)"

    # 00director.rpy:1602
    old "(filename)"
    new "(filename)"

    # 00director.rpy:1631
    old "Change"
    new "Change"

    # 00director.rpy:1633
    old "Add"
    new "Add"

    # 00director.rpy:1639
    old "Remove"
    new "Remove"

    # 00director.rpy:1674
    old "Statement:"
    new "Statement:"

    # 00director.rpy:1695
    old "Tag:"
    new "Tag:"

    # 00director.rpy:1711
    old "Attributes:"
    new "Attributes:"

    # 00director.rpy:1729
    old "Transforms:"
    new "Transforms:"

    # 00director.rpy:1748
    old "Behind:"
    new "Behind:"

    # 00director.rpy:1767
    old "Transition:"
    new "Transition:"

    # 00director.rpy:1785
    old "Channel:"
    new "Channel:"

    # 00director.rpy:1803
    old "Audio Filename:"
    new "Audio Filename:"

    # 00keymap.rpy:261
    old "Failed to save screenshot as %s."
    new "Failed to save screenshot as %s."

    # 00library.rpy:179
    old "bar"
    new "bar"

    # 00library.rpy:180
    old "selected"
    new "selected"

    # 00library.rpy:181
    old "viewport"
    new "viewport"

    # 00library.rpy:182
    old "horizontal scroll"
    new "horizontal scroll"

    # 00library.rpy:183
    old "vertical scroll"
    new "vertical scroll"

    # 00library.rpy:184
    old "activate"
    new "activate"

    # 00library.rpy:185
    old "deactivate"
    new "deactivate"

    # 00library.rpy:186
    old "increase"
    new "increase"

    # 00library.rpy:187
    old "decrease"
    new "decrease"

    # 00preferences.rpy:233
    old "display"
    new "display"

    # 00preferences.rpy:245
    old "transitions"
    new "transitions"

    # 00preferences.rpy:254
    old "skip transitions"
    new "skip transitions"

    # 00preferences.rpy:256
    old "video sprites"
    new "video sprites"

    # 00preferences.rpy:265
    old "show empty window"
    new "show empty window"

    # 00preferences.rpy:274
    old "text speed"
    new "text speed"

    # 00preferences.rpy:282
    old "joystick"
    new "joystick"

    # 00preferences.rpy:282
    old "joystick..."
    new "joystick..."

    # 00preferences.rpy:289
    old "skip"
    new "skip"

    # 00preferences.rpy:292
    old "skip unseen [text]"
    new "skip unseen [text]"

    # 00preferences.rpy:297
    old "skip unseen text"
    new "skip unseen text"

    # 00preferences.rpy:299
    old "begin skipping"
    new "begin skipping"

    # 00preferences.rpy:303
    old "after choices"
    new "after choices"

    # 00preferences.rpy:310
    old "skip after choices"
    new "skip after choices"

    # 00preferences.rpy:312
    old "auto-forward time"
    new "auto-forward time"

    # 00preferences.rpy:326
    old "auto-forward"
    new "auto-forward"

    # 00preferences.rpy:336
    old "auto-forward after click"
    new "auto-forward after click"

    # 00preferences.rpy:345
    old "automatic move"
    new "automatic move"

    # 00preferences.rpy:354
    old "wait for voice"
    new "wait for voice"

    # 00preferences.rpy:363
    old "voice sustain"
    new "voice sustain"

    # 00preferences.rpy:372
    old "self voicing"
    new "self voicing"

    # 00preferences.rpy:381
    old "clipboard voicing"
    new "clipboard voicing"

    # 00preferences.rpy:390
    old "debug voicing"
    new "debug voicing"

    # 00preferences.rpy:399
    old "emphasize audio"
    new "emphasize audio"

    # 00preferences.rpy:408
    old "rollback side"
    new "rollback side"

    # 00preferences.rpy:418
    old "gl powersave"
    new "gl powersave"

    # 00preferences.rpy:424
    old "gl framerate"
    new "gl framerate"

    # 00preferences.rpy:427
    old "gl tearing"
    new "gl tearing"

    # 00preferences.rpy:430
    old "font transform"
    new "font transform"

    # 00preferences.rpy:433
    old "font size"
    new "font size"

    # 00preferences.rpy:441
    old "font line spacing"
    new "font line spacing"

    # 00preferences.rpy:460
    old "music volume"
    new "music volume"

    # 00preferences.rpy:461
    old "sound volume"
    new "sound volume"

    # 00preferences.rpy:462
    old "voice volume"
    new "voice volume"

    # 00preferences.rpy:463
    old "mute music"
    new "mute music"

    # 00preferences.rpy:464
    old "mute sound"
    new "mute sound"

    # 00preferences.rpy:465
    old "mute voice"
    new "mute voice"

    # 00preferences.rpy:466
    old "mute all"
    new "mute all"

    # _compat\gamemenu.rpym:198
    old "Empty Slot."
    new "Empty Slot."

    # _compat\gamemenu.rpym:355
    old "Previous"
    new "Previous"

    # _compat\gamemenu.rpym:362
    old "Next"
    new "Next"

    # _compat\preferences.rpym:428
    old "Joystick Mapping"
    new "Joystick Mapping"

    # _developer\developer.rpym:38
    old "Developer Menu"
    new "Developer Menu"

    # _developer\developer.rpym:43
    old "Interactive Director (D)"
    new "Interactive Director (D)"

    # _developer\developer.rpym:45
    old "Reload Game (Shift+R)"
    new "Reload Game (Shift+R)"

    # _developer\developer.rpym:47
    old "Console (Shift+O)"
    new "Console (Shift+O)"

    # _developer\developer.rpym:49
    old "Variable Viewer"
    new "Variable Viewer"

    # _developer\developer.rpym:51
    old "Image Location Picker"
    new "Image Location Picker"

    # _developer\developer.rpym:53
    old "Filename List"
    new "Filename List"

    # _developer\developer.rpym:57
    old "Show Image Load Log (F4)"
    new "Show Image Load Log (F4)"

    # _developer\developer.rpym:60
    old "Hide Image Load Log (F4)"
    new "Hide Image Load Log (F4)"

    # _developer\developer.rpym:63
    old "Image Attributes"
    new "Image Attributes"

    # _developer\developer.rpym:90
    old "[name] [attributes] (hidden)"
    new "[name] [attributes] (hidden)"

    # _developer\developer.rpym:94
    old "[name] [attributes]"
    new "[name] [attributes]"

    # _developer\developer.rpym:143
    old "Nothing to inspect."
    new "Nothing to inspect."

    # _developer\developer.rpym:154
    old "Hide deleted"
    new "Hide deleted"

    # _developer\developer.rpym:154
    old "Show deleted"
    new "Show deleted"

    # _developer\developer.rpym:278
    old "Return to the developer menu"
    new "Return to the developer menu"

    # _developer\developer.rpym:443
    old "Rectangle: %r"
    new "Rectangle: %r"

    # _developer\developer.rpym:448
    old "Mouse position: %r"
    new "Mouse position: %r"

    # _developer\developer.rpym:453
    old "Right-click or escape to quit."
    new "Right-click or escape to quit."

    # _developer\developer.rpym:485
    old "Rectangle copied to clipboard."
    new "Rectangle copied to clipboard."

    # _developer\developer.rpym:488
    old "Position copied to clipboard."
    new "Position copied to clipboard."

    # _developer\developer.rpym:507
    old "Type to filter: "
    new "Type to filter: "

    # _developer\developer.rpym:635
    old "Textures: [tex_count] ([tex_size_mb:.1f] MB)"
    new "Textures: [tex_count] ([tex_size_mb:.1f] MB)"

    # _developer\developer.rpym:639
    old "Image cache: [cache_pct:.1f]% ([cache_size_mb:.1f] MB)"
    new "Image cache: [cache_pct:.1f]% ([cache_size_mb:.1f] MB)"

    # _developer\developer.rpym:649
    old "✔ "
    new "✔ "

    # _developer\developer.rpym:652
    old "✘ "
    new "✘ "

    # _developer\developer.rpym:657
    old "\n{color=#cfc}✔ predicted image (good){/color}\n{color=#fcc}✘ unpredicted image (bad){/color}\n{color=#fff}Drag to move.{/color}"
    new "\n{color=#cfc}✔ predicted image (good){/color}\n{color=#fcc}✘ unpredicted image (bad){/color}\n{color=#fff}Drag to move.{/color}"

    # _developer\inspector.rpym:38
    old "Displayable Inspector"
    new "Displayable Inspector"

    # _developer\inspector.rpym:61
    old "Size"
    new "Size"

    # _developer\inspector.rpym:65
    old "Style"
    new "Style"

    # _developer\inspector.rpym:71
    old "Location"
    new "Location"

    # _developer\inspector.rpym:122
    old "Inspecting Styles of [displayable_name!q]"
    new "Inspecting Styles of [displayable_name!q]"

    # _developer\inspector.rpym:139
    old "displayable:"
    new "displayable:"

    # _developer\inspector.rpym:145
    old "        (no properties affect the displayable)"
    new "        (no properties affect the displayable)"

    # _developer\inspector.rpym:147
    old "        (default properties omitted)"
    new "        (default properties omitted)"

    # _developer\inspector.rpym:185
    old "<repr() failed>"
    new "<repr() failed>"

    # _layout\classic_load_save.rpym:170
    old "a"
    new "a"

    # _layout\classic_load_save.rpym:179
    old "q"
    new "q"

    # 00gltest.rpy:70
    old "Renderer"
    new "Renderer"

    # 00gltest.rpy:93
    old "NPOT"
    new "NPOT"

    # 00gltest.rpy:131
    old "Powersave"
    new "Powersave"

    # 00gltest.rpy:145
    old "Framerate"
    new "Framerate"

    # 00gltest.rpy:149
    old "Screen"
    new "Screen"

    # 00gltest.rpy:153
    old "60"
    new "60"

    # 00gltest.rpy:157
    old "30"
    new "30"

    # 00gltest.rpy:163
    old "Tearing"
    new "Tearing"

    # _errorhandling.rpym:542
    old "Copy BBCode"
    new "Copy BBCode"

    # _errorhandling.rpym:544
    old "Copies the traceback.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."
    new "Copies the traceback.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."

    # _errorhandling.rpym:546
    old "Copy Markdown"
    new "Copy Markdown"

    # _errorhandling.rpym:548
    old "Copies the traceback.txt file to the clipboard as Markdown for Discord."
    new "Copies the traceback.txt file to the clipboard as Markdown for Discord."

    # _errorhandling.rpym:606
    old "Ignores the exception, allowing you to continue."
    new "Ignores the exception, allowing you to continue."

    # _errorhandling.rpym:683
    old "Copies the errors.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."
    new "Copies the errors.txt file to the clipboard as BBcode for forums like https://lemmasoft.renai.us/."

    # _errorhandling.rpym:687
    old "Copies the errors.txt file to the clipboard as Markdown for Discord."
    new "Copies the errors.txt file to the clipboard as Markdown for Discord."

