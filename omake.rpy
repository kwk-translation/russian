﻿# TODO: Translation updated at 2017-10-22 11:08

translate russian strings:

    old "Sakura"
    new "Сакура"

    old "Rika"
    new "Рика"

    old "Nanami"
    new "Нанами"

    # omake.rpy:82
    old "{b}D.O.B.:{/b} 1978/09/29\n"
    new "{b}Д.Р.:{/b} 1978/09/29\n"

    # omake.rpy:83
    old "{b}P.O.B.:{/b} Shinjuku, Tokyo\n"
    new "{b}М.Р.:{/b} Синдзюку, Токио\n"

    # omake.rpy:84
    old "{b}Height:{/b} 5.4ft\n"
    new "{b}Рост:{/b} 5.4фт\n"

    # omake.rpy:85
    old "{b}Weight:{/b} 136 pounds\n"
    new "{b}Вес:{/b} 136 фунтов\n"

    # omake.rpy:86
    old "{b}Measurements:{/b} 74-64-83\n"
    new "{b}Размеры:{/b} 74-64-83\n"

    # omake.rpy:87
    old "{b}Blood type:{/b} A\n"
    new "{b}Группа крови:{/b} 2\n"

    # omake.rpy:88
    old "{b}Favourite manga:{/b} High School Samurai\n"
    new "{b}Любимая манга:{/b} Самурай из старшей школы\n"

    # omake.rpy:89
    old "{b}Favourite videogame:{/b} Lead of Fighters ‘96\n"
    new "{b}Любимая видеоигра:{/b} Десница Бойца ‘96\n"

    # omake.rpy:90
    old "{b}Favourite food:{/b} American hamburgers\n"
    new "{b}Любимая еда:{/b} Гамбургеры\n"

    # omake.rpy:114
    old "{b}D.O.B.:{/b} 1979/02/28\n"
    new "{b}Д.Р.:{/b} 1979/02/28\n"

    # omake.rpy:115
    old "{b}P.O.B.:{/b} Kameoka, Kyoto\n"
    new "{b}М.Р.:{/b} Камеока, Киото\n"

    # omake.rpy:116
    old "{b}Height:{/b} 5.1ft\n"
    new "{b}Рост:{/b} 5.1фт\n"

    # omake.rpy:117
    old "{b}Weight:{/b} 121 pounds\n"
    new "{b}Вес:{/b} 121 фунт\n"

    # omake.rpy:118
    old "{b}Measurements:{/b} Unknown\n"
    new "{b}Размеры:{/b} Неизвестно\n"

    # omake.rpy:119
    old "{b}Blood type:{/b} AB\n"
    new "{b}Группа крови:{/b} 4\n"

    # omake.rpy:120
    old "{b}Favourite manga:{/b} Uchuu Tenshi Moechan\n"
    new "{b}Любимая манга:{/b} Вселенная Ангела\n"

    # omake.rpy:121
    old "{b}Favourite videogame:{/b} Taiko no Masuta EX 4’\n"
    new "{b}Любимая видеоигра:{/b} Мастер Масута Х 4’\n"

    # omake.rpy:122
    old "{b}Favourite food:{/b} Beef yakitori\n"
    new "{b}Любимая еда:{/b} Якитори из говядины\n"

    # omake.rpy:123
    old "Sakura is a member of the school's manga club and she has a very deep secret that makes of her a mysterious girl...\nShe is very shy but incredibly pretty. She was the idol of the school until a strange rumor about her started to spread. She likes classical music and plays violin sometimes in the night at her window..."
    new "Сакура - член клуба манги, скрывающая один маленький секрет, делающий её самой таинственной девочкой в школе...\nОна очень застенчива, но необычайно красива. Когда-то была всеобщим кумиром, пока не появились странные слухи о её личности. Любит классическую музыку, а вечерами, время от времени, играет на скрипке у окна..."

    # omake.rpy:146
    old "{b}D.O.B.:{/b} 1979/08/05\n"
    new "{b}Д.Р.:{/b} 1979/08/05\n"

    # omake.rpy:147
    old "{b}P.O.B.:{/b} The Village, Osaka\n"
    new "{b}М.Р.:{/b} Деревня, Осака\n"

    # omake.rpy:148
    old "{b}Height:{/b} 5ft\n"
    new "{b}Рост:{/b} 5фт\n"

    # omake.rpy:149
    old "{b}Weight:{/b} 110 pounds\n"
    new "{b}Вес:{/b} 110 фунтов\n"

    # omake.rpy:150
    old "{b}Measurements:{/b} 92-64-87\n"
    new "{b}Размеры:{/b} 92-64-87\n"

    # omake.rpy:151
    old "{b}Blood type:{/b} O\n"
    new "{b}Группа крови:{/b} 1\n"

    # omake.rpy:152
    old "{b}Favourite manga:{/b} Rosario Maiden\n"
    new "{b}Любимая манга:{/b} Девы Розарии\n"

    # omake.rpy:153
    old "{b}Favourite videogame:{/b} Super Musashi Galaxy Fight\n"
    new "{b}Любимая видеоигра:{/b} Галактическая Битва Супер Мусаси\n"

    # omake.rpy:154
    old "{b}Favourite food:{/b} Takoyaki\n"
    new "{b}Любимая еда:{/b} Такояки\n"

    # omake.rpy:155
    old "Rika is the founder of the manga club.\nShe got very bad experiences with boys and she sees them as perverts since then. Rika cosplays as a hobby and her best and favourite cosplay is the heroine of the Domoco-chan anime. She have strange eyes minnows that makes every boys dreamy. She speaks in the Kansai dialect like most of the people originating from the Village.\nShe secretly have a little crush on Sakura..."
    new "Рика - основатель клуба манги.\nИмея за плечами плохой опыт с мальчиками, уверенно считает их извращенцами. Её хобби - косплей, но лучше всего она косплеит героиню аниме Домоко-чан. У неё разные глаза, что привлекает немало отважных мечтателей. Говорит на Кансайском диалекте, впрочем, как и большинство жителей деревушки.\nИмеет тайные счёты с Сакурой... "

    # omake.rpy:178
    old "{b}D.O.B.:{/b} 1980/10/11\n"
    new "{b}Д.Р.:{/b} 1980/10/11\n"

    # omake.rpy:179
    old "{b}P.O.B.:{/b} Ginoza, Okinawa\n"
    new "{b}М.Р.:{/b} Гинодза, Окинава\n"

    # omake.rpy:180
    old "{b}Height:{/b} 4.5ft\n"
    new "{b}Рост:{/b} 4.5фт\n"

    # omake.rpy:181
    old "{b}Weight:{/b} 99 pounds\n"
    new "{b}Вес:{/b} 99 фунтов\n"

    # omake.rpy:182
    old "{b}Measurements:{/b} 71-51-74\n"
    new "{b}Размеры:{/b} 71-51-74\n"

    # omake.rpy:183
    old "{b}Blood type:{/b} B\n"
    new "{b}Группа крови:{/b} 3\n"

    # omake.rpy:184
    old "{b}Favourite manga:{/b} Nanda no Ryu\n"
    new "{b}Любимая манга:{/b} Рю Нанда\n"

    # omake.rpy:185
    old "{b}Favourite videogame:{/b} Pika Pika Rocket\n"
    new "{b}Любимая видеоигра:{/b} Ракета Пики\n"

    # omake.rpy:186
    old "{b}Favourite food:{/b} Chanpuruu\n"
    new "{b}Любимая еда:{/b} Тямпуру\n"

    # omake.rpy:187
    old "Nanami lives alone with her big brother Toshio after their parents disappeared.\nShe's a quiet introvert girl at first glance, but once she's with her friends, she's a cute energy bomb. She has a natural talent for videogames, which made of her the champion of the Osaka prefecture in numerous videogames."
    new "Нанами живёт вместе со своим братом с тех пор, как её родители исчезли. \nНа первый взгляд кажется неисправимым интровертом, но в присутствии друзей превращается во взрывного экстраверта. Имеет талант в видеоиграх, сделавший её чемпионом префектуры Осака во множестве игр."

    old "A young boy from Tokyo who has just moved to the village. At first, he thinks he's going to miss the urban life he knew before. But meeting Sakura will quickly change his mind...\nHe is a nice guy, determined, and sometimes a little bit crazy. He likes computers, mangas and loves to have fun with his friends. He is not afraid to face problems, especially when the sake of his friends is involved. He is quite uncertain on big decisions so he usually lets his instinct (or the player!) leading his decisions most of the time...\nHe got an older sister that is married and still lives in Tokyo."
    new "Подросток из Токио, только что переехавший в деревню. В начале предполагает, что будет скучать по городской суете, знакомой ему ранее. Но знакомство с Сакурой переломит эту мысль...\nВесёлый и решительный парень, а порой и сумасшедший. Любит компьютеры, мангу и весёлое времяпровождение с друзьями. Стирает проблемы в пыль, особенно когда они касаются близких. Любит поразмышлять над разными вопросами, но обычно, они решаются веяниями инстинкта (или игрока!)...\nИмеет старшую сестру, которая замужем и до сих пор проживает в Токио."

    # omake.rpy:85
    old "Opening song \"TAKE MY HEART\""
    new "Opening song \"TAKE MY HEART\""

    # omake.rpy:86
    old "Performed by Mew Nekohime\nLyrics by Max le Fou and Masaki Deguchi\nComposed and sequenced by Max le Fou\n© {a=http://www.maxlefou.com/}JMF{/a} 2018"
    new "Performed by Mew Nekohime\nLyrics by Max le Fou and Masaki Deguchi\nComposed and sequenced by Max le Fou\n© {a=http://www.maxlefou.com/}JMF{/a} 2018"

    # omake.rpy:92
    old "{b}Japanese lyrics:{/b}\n"
    new "{b}Japanese lyrics:{/b}\n"

    # omake.rpy:124
    old "{b}Translation:{/b}\n"
    new "{b}Translation:{/b}\n"

    # omake.rpy:125
    old "When you take my hand, I feel I could fly"
    new "When you take my hand, I feel I could fly"

    # omake.rpy:126
    old "When I dive into your eyes, I feel I could drown in happiness\n"
    new "When I dive into your eyes, I feel I could drown in happiness\n"

    # omake.rpy:127
    old "We sure look different"
    new "We sure look different"

    # omake.rpy:128
    old "But despite that, my heart beats loud\n"
    new "But despite that, my heart beats loud\n"

    # omake.rpy:129
    old "A boy and a girl"
    new "A boy and a girl"

    # omake.rpy:130
    old "I'm just a human"
    new "I'm just a human"

    # omake.rpy:131
    old "I hope you don't mind"
    new "I hope you don't mind"

    # omake.rpy:132
    old "I can't control my feelings"
    new "I can't control my feelings"

    # omake.rpy:133
    old "Come to me, take my heart"
    new "Come to me, take my heart"

    # omake.rpy:134
    old "I will devote myself to you"
    new "I will devote myself to you"

    # omake.rpy:135
    old "No matter what, I love you"
    new "No matter what, I love you"
# TODO: Translation updated at 2019-04-23 11:21

translate russian strings:

    # omake.rpy:120
    old "{b}Romaji:{/b}\n"
    new "{b}Romaji:{/b}\n"
# TODO: Translation updated at 2019-04-27 11:15

translate russian strings:

    # omake.rpy:278
    old "Chapter 1"
    new "Chapter 1"

    # omake.rpy:278
    old "Complete the first chapter"
    new "Complete the first chapter"

    # omake.rpy:278
    old "Chapter 2"
    new "Chapter 2"

    # omake.rpy:278
    old "Complete the second chapter"
    new "Complete the second chapter"

    # omake.rpy:278
    old "Chapter 3"
    new "Chapter 3"

    # omake.rpy:278
    old "Complete the third chapter"
    new "Complete the third chapter"

    # omake.rpy:278
    old "Chapter 4"
    new "Chapter 4"

    # omake.rpy:278
    old "Complete the fourth chapter"
    new "Complete the fourth chapter"

    # omake.rpy:278
    old "Chapter 5"
    new "Chapter 5"

    # omake.rpy:278
    old "Complete the fifth chapter"
    new "Complete the fifth chapter"

    # omake.rpy:278
    old "Finally together"
    new "Finally together"

    # omake.rpy:278
    old "Finish the game for the first time"
    new "Finish the game for the first time"

    # omake.rpy:278
    old "The perfume of rice fields"
    new "The perfume of rice fields"

    # omake.rpy:278
    old "Get a kiss from Sakura"
    new "Get a kiss from Sakura"

    # omake.rpy:278
    old "It's not that I like you, baka!"
    new "It's not that I like you, baka!"

    # omake.rpy:278
    old "Get a kiss from Rika"
    new "Get a kiss from Rika"

    # omake.rpy:278
    old "A new kind of game"
    new "A new kind of game"

    # omake.rpy:278
    old "Get a kiss from Nanami"
    new "Get a kiss from Nanami"

    # omake.rpy:278
    old "It's a trap!"
    new "It's a trap!"

    # omake.rpy:278
    old "Find the truth about Sakura"
    new "Find the truth about Sakura"

    # omake.rpy:278
    old "Good guy"
    new "Good guy"

    # omake.rpy:278
    old "Tell Sakura the truth about the yukata"
    new "Tell Sakura the truth about the yukata"

    # omake.rpy:278
    old "It's all about the Pentiums, baby"
    new "It's all about the Pentiums, baby"

    # omake.rpy:278
    old "Choose to game at the beginning of the game."
    new "Choose to game at the beginning of the game."

    # omake.rpy:278
    old "She got me!"
    new "She got me!"

    # omake.rpy:278
    old "Help Rika with the festival"
    new "Help Rika with the festival"

    # omake.rpy:278
    old "Grope!"
    new "Grope!"

    # omake.rpy:278
    old "Grope Rika (accidentally)"
    new "Grope Rika (accidentally)"

    # omake.rpy:278
    old "A good prank"
    new "A good prank"

    # omake.rpy:278
    old "Prank your friends with Nanami"
    new "Prank your friends with Nanami"

    # omake.rpy:278
    old "I'm not little!"
    new "I'm not little!"

    # omake.rpy:278
    old "Help Sakura tell the truth to Nanami"
    new "Help Sakura tell the truth to Nanami"

    # omake.rpy:278
    old "Big change of life"
    new "Big change of life"

    # omake.rpy:278
    old "Complete Sakura's route"
    new "Complete Sakura's route"

    # omake.rpy:278
    old "City Rat"
    new "City Rat"

    # omake.rpy:278
    old "Complete Rika's route"
    new "Complete Rika's route"

    # omake.rpy:278
    old "That new girl"
    new "That new girl"

    # omake.rpy:278
    old "Complete Nanami's route"
    new "Complete Nanami's route"

    # omake.rpy:278
    old "Knee-Deep into the 34D"
    new "Knee-Deep into the 34D"

    # omake.rpy:278
    old "Find the secret code in the game"
    new "Find the secret code in the game"

