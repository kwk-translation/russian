﻿# TODO: Translation updated at 2017-10-07 10:37

translate russian strings:

    # screens.rpy:253
    old "Back"
    new "Назад"

    # screens.rpy:254
    old "History"
    new "История"

    # screens.rpy:255
    old "Skip"
    new "Пропуск"

    # screens.rpy:256
    old "Auto"
    new "Авто"

    # screens.rpy:257
    old "Save"
    new "Сохранение"

    # screens.rpy:258
    old "Q.save"
    new "Б.сохранение"

    # screens.rpy:259
    old "Q.load"
    new "Б.загрузка"

    # screens.rpy:260
    old "Settings"
    new "Настройки"

    # screens.rpy:302
    old "New game"
    new "Новая игра"

    # screens.rpy:310
    old "Load"
    new "Загрузка"

    # screens.rpy:313
    old "Bonus"
    new "Бонусы"

    # screens.rpy:319
    old "End Replay"
    new "Остановить Воспр."

    # screens.rpy:323
    old "Main menu"
    new "Главное меню"

    # screens.rpy:325
    old "About"
    new "О нас"

    # screens.rpy:327
    old "Please donate!"
    new "Пожертвуйте" 

    # screens.rpy:332
    old "Help"
    new "Помощь"

    # screens.rpy:336
    old "Quit"
    new "Выход"

    # screens.rpy:387
    old "Update Available!"
    new "Доступно\nОбновление!"

    # screens.rpy:562
    old "Version [config.version!t]\n"
    new "Версия [config.version!t]\n"

    # screens.rpy:568
    old "Ren'Py version: [renpy.version_only].\n\n[renpy.license!t]"
    new "Версия Ren'Py: [renpy.version_only].\n\n[renpy.license!t]"

    # screens.rpy:608
    old "Page {}"
    new "Страница {}"

    # screens.rpy:608
    old "Automatic saves"
    new "Автосохранения"

    # screens.rpy:608
    old "Quick saves"
    new "Быстрые сохранения"

    # screens.rpy:650
    old "{#file_time}%A, %B %d %Y, %H:%M"
    new "{#file_time}%A, %B %d %Y, %H:%M"

    # screens.rpy:650
    old "empty slot"
    new "пустой слот"

    # screens.rpy:667
    old "<"
    new "<"

    # screens.rpy:670
    old "{#auto_page}A"
    new "{#auto_page}A"

    # screens.rpy:673
    old "{#quick_page}Q"
    new "{#quick_page}Q"

    # screens.rpy:679
    old ">"
    new ">"

    # screens.rpy:741
    old "Display"
    new "Экран"

    # screens.rpy:742
    old "Windowed"
    new "Оконный режим"

    # screens.rpy:743
    old "Fullscreen"
    new "Полный экран"

    # screens.rpy:747
    old "Rollback Side"
    new "Откаты"

    # screens.rpy:748
    old "Disable"
    new "Выключить"

    # screens.rpy:749
    old "Left"
    new "Слева"

    # screens.rpy:750
    old "Right"
    new "Справа"

    # screens.rpy:755
    old "Unseen Text"
    new "Невиданный Текст"

    # screens.rpy:756
    old "After choices"
    new "После выбора"

    # screens.rpy:757
    old "Transitions"
    new "Переходы"

    # screens.rpy:772
    old "Text speed"
    new "Скорость текста"

    # screens.rpy:776
    old "Auto forward"
    new "Автом. перемотка"

    # screens.rpy:783
    old "Music volume"
    new "Громкость музыки"

    # screens.rpy:790
    old "Sound volume"
    new "Громкость звука"

    # screens.rpy:796
    old "Test"
    new "Тест"

    # screens.rpy:800
    old "Voice volume"
    new "Громкость голосов"

    # screens.rpy:811
    old "Mute All"
    new "Отключить Звук"

    # screens.rpy:822
    old "Language"
    new "Язык"

    # screens.rpy:948
    old "The dialogue history is empty."
    new "История диалогов пуста."

    # screens.rpy:1013
    old "Keyboard"
    new "Клавиатура"

    # screens.rpy:1014
    old "Mouse"
    new "Мышь"

    # screens.rpy:1017
    old "Gamepad"
    new "Геймпад"

    # screens.rpy:1030
    old "Enter"
    new "Кл.Ввод"

    # screens.rpy:1031
    old "Advances dialogue and activates the interface."
    new "Продвигает диалоги далее и активирует элементы интерфейса."

    # screens.rpy:1034
    old "Space"
    new "Кл.Пробел"

    # screens.rpy:1035
    old "Advances dialogue without selecting choices."
    new "Продвигает диалоги далее, но не работает на моментах выбора."

    # screens.rpy:1038
    old "Arrow Keys"
    new "Кл.Стрелок"

    # screens.rpy:1039
    old "Navigate the interface."
    new "Перемещают по интерфейсу."

    # screens.rpy:1042
    old "Escape"
    new "Кл.выход"

    # screens.rpy:1043
    old "Accesses the game menu."
    new "Выводит на экран меню игры."

    # screens.rpy:1046
    old "Ctrl"
    new "Кл.Ctrl"

    # screens.rpy:1047
    old "Skips dialogue while held down."
    new "Пропускает диалоги так долго, пока нажата."

    # screens.rpy:1050
    old "Tab"
    new "Кл.Tab"

    # screens.rpy:1051
    old "Toggles dialogue skipping."
    new "Запускает пропуск диалогов."

    # screens.rpy:1054
    old "Page Up"
    new "Кл.Page Up"

    # screens.rpy:1055
    old "Rolls back to earlier dialogue."
    new "Возвращает на более ранние диалоги."

    # screens.rpy:1058
    old "Page Down"
    new "Кл.Page Down"

    # screens.rpy:1059
    old "Rolls forward to later dialogue."
    new "Перемещает на несколько диалогов вперёд."

    # screens.rpy:1063
    old "Hides the user interface."
    new "Скрывает пользовательский интерфейс."

    # screens.rpy:1067
    old "Takes a screenshot."
    new "Делает скриншот."

    # screens.rpy:1071
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new "Включает {a=https://www.renpy.org/l/voicing}синтез речи{/a}."

    # screens.rpy:1077
    old "Left Click"
    new "Клик по левой Кл.Мыши"

    # screens.rpy:1081
    old "Middle Click"
    new "Клик по Колёсику Мыши"

    # screens.rpy:1085
    old "Right Click"
    new "Клик по правой Кл.Мыши"

    # screens.rpy:1089
    old "Mouse Wheel Up\nClick Rollback Side"
    new "Колёсико Вверх\nНажатие Отката"

    # screens.rpy:1093
    old "Mouse Wheel Down"
    new "Колёсико Вниз"

    # screens.rpy:1100
    old "Right Trigger\nA/Bottom Button"
    new "Правый Триггер\nA/Нижняя Кнопка"

    # screens.rpy:1104
    old "Left Trigger\nLeft Shoulder"
    new "Левый Триггер\nЛевая Плечевая Кнопка"

    # screens.rpy:1108
    old "Right Shoulder"
    new "Правая Плечевая Кнопка"

    # screens.rpy:1112
    old "D-Pad, Sticks"
    new "D-Pad, Стики"

    # screens.rpy:1116
    old "Start, Guide"
    new "Старт, Навигация"

    # screens.rpy:1120
    old "Y/Top Button"
    new "Y/Верхняя Кнопка"

    # screens.rpy:1123
    old "Calibrate"
    new "Калибровка"

    # screens.rpy:1188
    old "Yes"
    new "Да"

    # screens.rpy:1189
    old "No"
    new "Нет"

    # screens.rpy:1235
    old "Skipping"
    new "Пропуск"

    # screens.rpy:1456
    old "Menu"
    new "Меню"

    # screens.rpy:1547
    old "Picture gallery"
    new "Галерея изображений"

    # screens.rpy:1548
    old "Music player"
    new "Проигрыватель"


# TODO: Translation updated at 2019-04-23 11:21

translate russian strings:

    # screens.rpy:1532
    old "Musics and pictures"
    new "Musics and pictures"

    # screens.rpy:1541
    old "Opening song lyrics"
    new "Opening song lyrics"

    # screens.rpy:1545
    old "Characters profiles"
    new "Characters profiles"

    # screens.rpy:1566
    old "Bonus chapters"
    new "Bonus chapters"

    # screens.rpy:1569
    old "1 - A casual day at the club"
    new "1 - A casual day at the club"

    # screens.rpy:1574
    old "2 - Questioning sexuality (Sakura's route)"
    new "2 - Questioning sexuality (Sakura's route)"

    # screens.rpy:1579
    old "3 - Headline news"
    new "3 - Headline news"

    # screens.rpy:1584
    old "4a - A picnic at the summer (Sakura's route)"
    new "4a - A picnic at the summer (Sakura's route)"

    # screens.rpy:1589
    old "4b - A picnic at the summer (Rika's route)"
    new "4b - A picnic at the summer (Rika's route)"

    # screens.rpy:1594
    old "4c - A picnic at the summer (Nanami's route)"
    new "4c - A picnic at the summer (Nanami's route)"

    # screens.rpy:1611
    old "Max le Fou - Taichi's Theme"
    new "Max le Fou - Taichi's Theme"

    # screens.rpy:1613
    old "Max le Fou - Sakura's Waltz"
    new "Max le Fou - Sakura's Waltz"

    # screens.rpy:1615
    old "Max le Fou - Rika's theme"
    new "Max le Fou - Rika's theme"

    # screens.rpy:1617
    old "Max le Fou - Of Bytes and Sanshin"
    new "Max le Fou - Of Bytes and Sanshin"

    # screens.rpy:1619
    old "Max le Fou - Time for School"
    new "Max le Fou - Time for School"

    # screens.rpy:1621
    old "Max le Fou - Sakura's secret"
    new "Max le Fou - Sakura's secret"

    # screens.rpy:1623
    old "Max le Fou - I think I'm in love"
    new "Max le Fou - I think I'm in love"

    # screens.rpy:1625
    old "Max le Fou - Darkness of the Village"
    new "Max le Fou - Darkness of the Village"

    # screens.rpy:1627
    old "Max le Fou - Love always win"
    new "Max le Fou - Love always win"

    # screens.rpy:1629
    old "Max le Fou - Ondo"
    new "Max le Fou - Ondo"

    # screens.rpy:1631
    old "Max le Fou - Happily Ever After"
    new "Max le Fou - Happily Ever After"

    # screens.rpy:1633
    old "J.S. Bach - Air Orchestral suite #3"
    new "J.S. Bach - Air Orchestral suite #3"

    # screens.rpy:1635
    old "Mew Nekohime - TAKE MY HEART (TV Size)"
    new "Mew Nekohime - TAKE MY HEART (TV Size)"

# TODO: Translation updated at 2019-04-27 11:15

translate russian strings:

    # screens.rpy:1552
    old "Achievements"
    new "Achievements"

